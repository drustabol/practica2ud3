CREATE DATABASE if not exists bdatos;
--
USE bdatos;
--
CREATE TABLE if not exists marca
(
    id          int auto_increment primary key,
    nombremarca varchar(30),
    pais        varchar(20),
    delegacion  varchar(30),
    web         varchar(50)
);
--
create table if not exists material
(
    id             int auto_increment primary key,
    nombreMaterial varchar(30),
    calidad        varchar(20),
    organico       varchar(2),
    procedencia    varchar(30)
);
--
CREATE TABLE if not exists propietario
(
    id        int auto_increment primary key,
    nombre    varchar(20),
    apellidos varchar(30),
    dccion    varchar(50),
    tfno      int(9)
);
--
CREATE TABLE if not exists instrumento
(
    id                int auto_increment primary key,
    codigo            varchar(10) not null unique,
    tipo              varchar(15),
    nombre            varchar(50),
    pvp               double,
    fecha_adquisicion date,
    idmarca           int,
    idmaterial        int,
    idpropietario     int,
    FOREIGN KEY (idMarca) REFERENCES marca (id),
    FOREIGN KEY (idMaterial) REFERENCES material (id),
    FOREIGN KEY (idPropietario) REFERENCES propietario (id)
);
--
CREATE TABLE if not exists instrumentoMateriales
(
    id 		  int auto_increment primary key,
    idinstrumento int,
    idmaterial    int,
    cantidad      int default 0,
    FOREIGN KEY (idInstrumento) REFERENCES instrumento (id) ,
    FOREIGN KEY (idMaterial) REFERENCES material (id)
);
--
CREATE TABLE if not exists marcainstrumento
(
    idinstrumento int,
    idmarca       int,
    FOREIGN KEY (idInstrumento) REFERENCES instrumento (id),
    FOREIGN KEY (idMarca) REFERENCES marca (id)
);
--
create table IF NOT exists marcaMaterial
(
    idmaterial int,
    idmarca    int,
    FOREIGN KEY (idMaterial) REFERENCES material (id),
    FOREIGN KEY (idMarca) REFERENCES marca (id)
);
