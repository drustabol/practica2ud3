import gui.Controlador;
import gui.Modelo;
import gui.Vista;

public class Principal {

    public static void main(final String[] args) throws Exception {
        Vista vista= new Vista();
        Modelo modelo= new Modelo();
        Controlador controlador= new Controlador(vista, modelo);

    }
}