package enums;

public enum Calidades {
    BUENA("Buena"),
    PMAESTRA("Pieza maestra"),
    COLECCIONISTA("Coleccionista"),
    PREMIUM("Premium");

    private  String valor;

    Calidades (String valor) {
        this.valor=valor;
    }

    public String getValor() {
        return valor;
    }
}
