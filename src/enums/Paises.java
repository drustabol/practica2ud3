package enums;

public enum Paises {
    ESPANA("España"),
    JAPON("Japón"),
    AUSTRIA("Austria"),
    ALEMANIA("Alemania"),
    CHINA("China"),
    EEUU("EEUU"),
    REPCHECA("República Checa"),
    UK("Reino Unido"),
    PBAJOS("Países Bajos"),
    ITALIA("italia");

    private String valor;

    Paises (String valor) {
        this.valor=valor;
    }

    public String getValor() {
        return valor;
    }
}
