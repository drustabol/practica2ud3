package enums;

public enum TipoInstrumentos {
    VIENTO("Viento"),
    PERCUSION("Percusion"),
    CUERDA("Cuerda"),
    ELECTRICO("Electrico");

    private String valor;

    TipoInstrumentos(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

}
