package gui;

import hibernate.*;
import util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener {

    private Vista vista;
    private Modelo modelo;

    public Controlador(Modelo modelo, Vista vista) {

        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.listaInstrumentos.addListSelectionListener(listener);
        vista.listaMarcas.addListSelectionListener(listener);
        vista.listaMateriales.addListSelectionListener(listener);
        vista.listaPropietarios.addListSelectionListener(listener);
        vista.listaFabricados.addListSelectionListener(listener);
        vista.ListaInstrumentosPropietario.addListSelectionListener(listener);
    }

    private void addActionListeners(ActionListener listener) {
        vista.addInstrumentobtn.addActionListener(listener);
        vista.addInstrumentobtn.setActionCommand("addInstrumentobtn");
        vista.eliminarInstrumentoBtn.addActionListener(listener);
        vista.eliminarInstrumentoBtn.setActionCommand("eliminarInstrumentoBtn");
        vista.modificarInstrumentosBtn.addActionListener(listener);
        vista.modificarInstrumentosBtn.setActionCommand("modificarInstrumentosBtn");
        vista.btnlistadoInstrumentos.addActionListener(listener);
        vista.btnlistadoInstrumentos.setActionCommand("btnlistadoInstrumentos");

        //Listener de materiales
        vista.addMaterialBtn.addActionListener(listener);
        vista.addMaterialBtn.setActionCommand("addMaterialBtn");
        vista.eliminarMaterialBtn.addActionListener(listener);
        vista.eliminarMaterialBtn.setActionCommand("eliminarMaterialBtn");
        vista.modificarMaterialBtn.addActionListener(listener);
        vista.modificarMaterialBtn.setActionCommand("modificarMaterialBtn");
        vista.rbtnNo.addActionListener(listener);
        vista.rbtnSi.addActionListener(listener);
        vista.btnlistadoMateriales.addActionListener(listener);
        vista.btnlistadoMateriales.setActionCommand("btnlistadoMateriales");

        //Listener de marcas
        vista.addMarcaBtn.addActionListener(listener);
        vista.addMarcaBtn.setActionCommand("addMarcaBtn");
        vista.eliminarMarcaBtn.addActionListener(listener);
        vista.eliminarMarcaBtn.setActionCommand("eliminarMarcaBtn");
        vista.modificarMarcaBtn.addActionListener(listener);
        vista.modificarMarcaBtn.setActionCommand("modificarMarcaBtn");
        vista.btnListadoMarcas.addActionListener(listener);
        vista.btnListadoMarcas.setActionCommand("btnListadoMarcas");

        //Listener de propietarios
        vista.addPropietarioBtn.addActionListener(listener);
        vista.addPropietarioBtn.setActionCommand("addPropietarioBtn");
        vista.modificarPropietarioBtn.addActionListener(listener);
        vista.modificarPropietarioBtn.setActionCommand("modificarPropietarioBtn");
        vista.eliminarPropietarioBtn.addActionListener(listener);
        vista.eliminarPropietarioBtn.setActionCommand("eliminarPropietarioBtn");
        vista.instrumentosBtn.addActionListener(listener);
        vista.instrumentosBtn.setActionCommand("instrumentosBtn");
        vista.btnlistadoPropietarios.addActionListener(listener);
        vista.btnlistadoPropietarios.setActionCommand("btnlistadoPropietarios");

        //Listener de Fabricación
        vista.insertarFabBtn.addActionListener(listener);
        vista.insertarFabBtn.setActionCommand("insertarFabBtn");
        vista.modificarFabBtn.addActionListener(listener);
        vista.modificarFabBtn.setActionCommand("modificarFabBtn");
        vista.borrarFabBtn.addActionListener(listener);
        vista.borrarFabBtn.setActionCommand("borrarFabBtn");
        vista.btnListadoFabricados.addActionListener(listener);
        vista.btnListadoFabricados.setActionCommand("btnListadoFabricados");

        //Listener menú
        vista.conexionItem.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        modelo = new Modelo();
        String comando = e.getActionCommand();

        switch (comando) {

            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "addInstrumentobtn":
                Instrumento nuevoInstrumento = new Instrumento();
                nuevoInstrumento.setCodigo(vista.txtCodigo.getText());
                nuevoInstrumento.setTipo(String.valueOf(vista.comboTipo.getSelectedItem()));
                nuevoInstrumento.setNombre(vista.txtNombre.getText());
                try {
                    nuevoInstrumento.setPvp(Double.parseDouble(vista.txtPvp.getText()));
                } catch (NumberFormatException ex) {
                    Util.showErrorAlert("Introduce números en precio");
                }
                nuevoInstrumento.setFechaAdquisicion(Date.valueOf(vista.datePicker.getDate()));
                nuevoInstrumento.setPropietario((Propietario) vista.comboPropietarios.getSelectedItem());

                modelo.insertar(nuevoInstrumento);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "modificarInstrumentosBtn":
                Instrumento instrumentoSeleccionado = (Instrumento) vista.listaInstrumentos.getSelectedValue();
                instrumentoSeleccionado.setCodigo(vista.txtCodigo.getText());
                instrumentoSeleccionado.setTipo(String.valueOf(vista.comboTipo.getSelectedItem()));
                instrumentoSeleccionado.setNombre(vista.txtNombre.getText());
                try {
                    instrumentoSeleccionado.setPvp(Double.parseDouble(vista.txtPvp.getText()));
                } catch (NumberFormatException e1) {
                    Util.showErrorAlert("Introduce números en precio");
                }
                instrumentoSeleccionado.setFechaAdquisicion(Date.valueOf(vista.datePicker.getDate()));
                instrumentoSeleccionado.setPropietario((Propietario) vista.comboPropietarios.getSelectedItem());
                modelo.modificar(instrumentoSeleccionado);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "eliminarInstrumentoBtn":
                Instrumento instrumentoBorrado = (Instrumento) vista.listaInstrumentos.getSelectedValue();
                modelo.eliminar(instrumentoBorrado);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "btnlistadoInstrumentos":
                listarInstrumentos(modelo.getInstrumentos());
                break;

            case "addMarcaBtn":
                Marca nuevaMarca = new Marca();
                nuevaMarca.setNombremarca(vista.txtNombreMarca.getText());
                nuevaMarca.setPais(String.valueOf(vista.comboPais.getSelectedItem()));
                nuevaMarca.setDelegacion(vista.txtDelegacion.getText());
                nuevaMarca.setWeb(vista.txtWeb.getText());
                modelo.insertar(nuevaMarca);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());

                break;

            case "modificarMarcaBtn":
                Marca marcaSeleccionada = (Marca) vista.listaMarcas.getSelectedValue();
                marcaSeleccionada.setNombremarca(vista.txtNombreMarca.getText());
                marcaSeleccionada.setPais(String.valueOf(vista.comboPais.getSelectedItem()));
                marcaSeleccionada.setDelegacion(vista.txtDelegacion.getText());
                marcaSeleccionada.setWeb(vista.txtWeb.getText());
                modelo.modificar(marcaSeleccionada);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());
                break;

            case "eliminarMarcaBtn":
                Marca marcaBorrada = (Marca) vista.listaMarcas.getSelectedValue();
                modelo.eliminar(marcaBorrada);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());
                break;

            case "btnListadoMarcas":
                listarMarcas(modelo.getMarcas());
                break;

            case "addMaterialBtn":
                Material nuevoMaterial = new Material();
                nuevoMaterial.setNombreMaterial(vista.txtNombreMaterial.getText());
                nuevoMaterial.setCalidad(String.valueOf(vista.comboCalidad.getSelectedItem()));
                if (vista.rbtnNo.isSelected()) {
                   nuevoMaterial.setOrganico("No");
                }
                if (vista.rbtnSi.isSelected()) {
                    nuevoMaterial.setOrganico("Si");
                }
                nuevoMaterial.setProcedencia(vista.txtProcedencia.getText());
                modelo.insertar(nuevoMaterial);
                limpiarCamposMateriales();
                listarMateriales(modelo.getMateriales());
                break;

            case "modificarMaterialBtn":
                Material materialSeleccionado = (Material) vista.listaMateriales.getSelectedValue();
                materialSeleccionado.setNombreMaterial(vista.txtNombreMaterial.getText());
                materialSeleccionado.setCalidad(String.valueOf(vista.comboCalidad.getSelectedItem()));
                if (vista.rbtnNo.isSelected()) {
                    materialSeleccionado.setOrganico("No");
                }
                if (vista.rbtnSi.isSelected()) {
                    materialSeleccionado.setOrganico("Si");
                }
                materialSeleccionado.setProcedencia(vista.txtProcedencia.getText());
                modelo.modificar(materialSeleccionado);
                limpiarCamposMateriales();
                listarMateriales(modelo.getMateriales());
                break;

            case "eliminarMaterialBtn":
                Material materialBorrado = (Material) vista.listaMateriales.getSelectedValue();
                modelo.eliminar(materialBorrado);
                limpiarCamposMarcas();
                listarMateriales(modelo.getMateriales());
                break;

            case "btnlistadoMateriales":
                listarMateriales(modelo.getMateriales());
                break;

            case "addPropietarioBtn":
                Propietario nuevoPropietario = new Propietario();
                nuevoPropietario.setNombre(vista.txtNombrePropietario.getText());
                nuevoPropietario.setApellidos(vista.txtApellidos.getText());
                nuevoPropietario.setDccion(vista.txtDccion.getText());
                nuevoPropietario.setTfno(Integer.parseInt(vista.txtTfno.getText()));
                modelo.insertar(nuevoPropietario);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());
                break;

            case "modificarPropietarioBtn":
                Propietario propietarioSeleccionado = (Propietario) vista.listaPropietarios.getSelectedValue();
                propietarioSeleccionado.setNombre(vista.txtNombrePropietario.getText());
                propietarioSeleccionado.setApellidos(vista.txtApellidos.getText());
                propietarioSeleccionado.setDccion(vista.txtDccion.getText());
                propietarioSeleccionado.setTfno(Integer.parseInt(vista.txtTfno.getText()));
                modelo.modificar(propietarioSeleccionado);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());

                break;

            case "eliminarPropietarioBtn":
                Propietario borrarPropietario = (Propietario) vista.listaPropietarios.getSelectedValue();
                modelo.eliminar(borrarPropietario);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());
                break;

            case "btnlistadoPropietarios":
                listarPropietarios(modelo.getPropietarios());
                break;

            case "insertarFabBtn":
                Instrumentomateriales nuevoFabricado = new Instrumentomateriales();
                try {
                    nuevoFabricado.setCantidad(Integer.parseInt(vista.txtCantidad.getText()));
                } catch (NumberFormatException ex) {
                    Util.showErrorAlert("Introduce números en precio");
                }
                nuevoFabricado.setInstrumento((Instrumento) vista.comboInstrumentos.getSelectedItem());
                nuevoFabricado.setMaterial((Material) vista.comboMaterial.getSelectedItem());
                modelo.insertar(nuevoFabricado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());

                break;

            case "modificarFabBtn":
                Instrumentomateriales fabricadoSeleccionado = (Instrumentomateriales) vista.listaFabricados.getSelectedValue();
                try {
                    fabricadoSeleccionado.setCantidad(Integer.parseInt(vista.txtCantidad.getText()));
                } catch (NumberFormatException ex) {
                    Util.showErrorAlert("Introduce números en precio");
                }
                fabricadoSeleccionado.setInstrumento((Instrumento) vista.comboInstrumentos.getSelectedItem());
                fabricadoSeleccionado.setMaterial((Material) vista.comboMaterial.getSelectedItem());
                modelo.modificar(fabricadoSeleccionado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());
                break;

            case "borrarFabBtn":
                Instrumentomateriales borrarFabricado = (Instrumentomateriales) vista.listaFabricados.getSelectedValue();
                modelo.eliminar(borrarFabricado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());
                break;

            case "btnListadoFabricados":
                listarFabricados(modelo.getFabricaciones());
                break;

            case "instrumentosBtn":
                Propietario prop= (Propietario) vista.listaPropietarios.getSelectedValue();
                prop.setInstrumento((ArrayList)modelo.getPropietariosInstrumento(prop));
                listarPropietarioInstrumentos(modelo.getPropietariosInstrumento(prop));
                break;

        }
        limpiarTodo();
        listarTodo();
    }

    private void limpiarTodo(){
        limpiarCamposPropietarios();
        limpiarCamposMarcas();
        limpiarCamposInstrumentos();
        limpiarCamposFabricados();
        limpiarCamposMateriales();
    }

    private void limpiarCamposFabricados() {
        vista.txtCantidad.setText("");
        vista.comboInstrumentos.setSelectedIndex(-1);
        vista.comboMaterial.setSelectedIndex(-1);

    }

    private void limpiarCamposPropietarios() {
        vista.txtNombrePropietario.setText("");
        vista.txtApellidos.setText("");
        vista.txtDccion.setText("");
        vista.txtTfno.setText("");
    }

    private void limpiarCamposMateriales() {
        vista.txtNombreMaterial.setText("");
        vista.comboCalidad.setSelectedIndex(-1);
        vista.rbtnSi.setSelected(false);
        vista.rbtnNo.setSelected(false);
        vista.txtProcedencia.setText("");
    }

    private void limpiarCamposMarcas() {
        vista.txtNombreMarca.setText("");
        vista.comboPais.setSelectedIndex(-1);
        vista.txtDelegacion.setText("");
        vista.txtWeb.setText("");
    }

    private void limpiarCamposInstrumentos() {
        vista.txtCodigo.setText("");
        vista.comboTipo.setSelectedIndex(-1);
        vista.txtNombre.setText("");
        vista.txtPvp.setText("");
        vista.datePicker.setText("");
        vista.comboPropietarios.setSelectedIndex(-1);
    }

    private void listarTodo(){
        listarInstrumentos(modelo.getInstrumentos());
        listarMateriales(modelo.getMateriales());
        listarMarcas(modelo.getMarcas());
        listarFabricados(modelo.getFabricaciones());
        listarPropietarios(modelo.getPropietarios());

    }

    private void listarInstrumentos(ArrayList<Instrumento> instrumentos) {
        vista.dlmInstrumentos.clear();
        for (Instrumento instrumento : instrumentos) {
            vista.dlmInstrumentos.addElement(instrumento);
        }

        vista.comboInstrumentos.removeAllItems();
        ArrayList<Instrumento> ins=modelo.getInstrumentos();
        for(Instrumento i: ins) {
            vista.comboInstrumentos.addItem(i);
        }
        vista.comboInstrumentos.setSelectedItem(-1);

    }

    public void listarMarcas(ArrayList<Marca> marcas) {
        vista.dlmMarca.clear();
        for (Marca marca : marcas) {
            vista.dlmMarca.addElement(marca);
        }
    }

    public void listarMateriales(ArrayList<Material> materiales) {
        vista.dlmMaterial.clear();
        for (Material material : materiales) {
            vista.dlmMaterial.addElement(material);
        }

        vista.comboMaterial.removeAllItems();
        ArrayList<Material> mat=modelo.getMateriales();
        for(Material m: mat) {
            vista.comboInstrumentos.addItem(m);
        }
        vista.comboMaterial.setSelectedItem(-1);
    }

    public void listarPropietarios(ArrayList<Propietario> propietarios) {
        vista.dlmPropietario.clear();
        for (Propietario propietario : propietarios) {
            vista.dlmPropietario.addElement(propietario);
        }

        vista.comboPropietarios.removeAllItems();
        ArrayList<Propietario> prop=modelo.getPropietarios();
        for (Propietario pr:prop){
            vista.comboPropietarios.addItem(pr);
        }
        vista.comboPropietarios.setSelectedItem(-1);
    }

    public void listarFabricados(ArrayList<Instrumentomateriales> fabricados) {
        vista.dlmFabricaciones.clear();
        for (Instrumentomateriales fabricado : fabricados) {
            vista.dlmFabricaciones.addElement(fabricado);
        }

        vista.comboInstrumentos.removeAllItems();
        ArrayList<Instrumento> ins=modelo.getInstrumentos();
        vista.comboMaterial.removeAllItems();
        ArrayList<Material> mat=modelo.getMateriales();
        for (Instrumento i: ins) {
            vista.comboInstrumentos.addItem(i);
        }
        for (Material m:mat) {
            vista.comboMaterial.addItem(m);
        }

        vista.comboInstrumentos.setSelectedItem(-1);
        vista.comboMaterial.setSelectedItem(-1);
    }

    public void listarPropietarioInstrumentos(List <Instrumento> lista){
        vista.dlmInstrumentosPropietario.clear();
        for (Instrumento instrumento: lista) {
            vista.dlmInstrumentosPropietario.addElement(instrumento);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listaInstrumentos) {
                Instrumento instrumento = (Instrumento) vista.listaInstrumentos.getSelectedValue();
                vista.txtCodigo.setText(String.valueOf(instrumento.getCodigo()));
                vista.comboTipo.setSelectedItem(instrumento.getTipo());
                vista.txtNombre.setText(String.valueOf(instrumento.getNombre()));
                vista.txtPvp.setText(String.valueOf(instrumento.getPvp()));
                vista.datePicker.setDate(instrumento.getFechaAdquisicion().toLocalDate());
                vista.comboPropietarios.setSelectedItem(instrumento.getPropietario());
            }else if(e.getSource()== vista.listaMarcas){
                Marca marca= (Marca) vista.listaMarcas.getSelectedValue();
                vista.txtNombreMarca.setText(String.valueOf(marca.getNombremarca()));
                vista.comboPais.setSelectedItem(marca.getPais());
                vista.txtDelegacion.setText(String.valueOf(marca.getDelegacion()));
                vista.txtWeb.setText(String.valueOf(marca.getWeb()));
            }else if(e.getSource()==vista.listaMateriales) {
                Material material= (Material) vista.listaMateriales.getSelectedValue();
                vista.txtNombreMaterial.setText(String.valueOf(material.getNombreMaterial()));
                vista.comboCalidad.setSelectedItem(material.getCalidad());
                if (vista.rbtnNo.isSelected()) vista.rbtnNo.setText("No");
                if (vista.rbtnSi.isSelected()) vista.rbtnSi.setText("Si");
                vista.txtProcedencia.setText(String.valueOf(material.getProcedencia()));
            }else if(e.getSource()==vista.listaPropietarios){
                Propietario propietario= (Propietario) vista.listaPropietarios.getSelectedValue();
                vista.txtNombrePropietario.setText(String.valueOf(propietario.getNombre()));
                vista.txtApellidos.setText(String.valueOf(propietario.getApellidos()));
                vista.txtDccion.setText(propietario.getDccion());
                vista.txtTfno.setText(String.valueOf(propietario.getTfno()));
            }else if(e.getSource()== vista.listaFabricados){
                Instrumentomateriales fabricado= (Instrumentomateriales) vista.listaFabricados.getSelectedValue();
                vista.txtCantidad.setText(String.valueOf(fabricado.getCantidad()));
                vista.comboInstrumentos.setSelectedItem(fabricado.getInstrumento());
                vista.comboMaterial.setSelectedItem(fabricado.getMaterial());
            }
        }
    }
}
