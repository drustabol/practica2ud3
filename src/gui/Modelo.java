package gui;

import hibernate.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {

    public static SessionFactory sessionFactory;

    public void desconectar() {
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    public void conectar() {
        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Instrumento.class);
        configuracion.addAnnotatedClass(Instrumentomateriales.class);
        configuracion.addAnnotatedClass(Marca.class);
        configuracion.addAnnotatedClass(Material.class);
        configuracion.addAnnotatedClass(Propietario.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(configuracion.getProperties()).build();
        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    ArrayList<Instrumento> getInstrumentos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Instrumento");
        ArrayList<Instrumento> lista = (ArrayList<Instrumento>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Marca> getMarcas(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Marca");
        ArrayList<Marca> lista = (ArrayList<Marca>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Material> getMateriales(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Material ");
        ArrayList<Material> lista = (ArrayList<Material>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Propietario> getPropietarios(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Propietario ");
        ArrayList<Propietario> lista = (ArrayList<Propietario>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Instrumentomateriales> getFabricaciones(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Instrumentomateriales ");
        ArrayList<Instrumentomateriales> lista = (ArrayList<Instrumentomateriales>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Instrumento> getPropietariosInstrumento(Propietario propietario){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Instrumento WHERE propietario =: edit");
        query.setParameter("edit", propietario);
        ArrayList<Instrumento> lista = (ArrayList<Instrumento>) query.getResultList();
        sesion.close();
        return lista;
    }

    void insertar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(o);
        sesion.getTransaction().commit();

        sesion.close();
    }

    void modificar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(o);
        sesion.getTransaction().commit();
        sesion.close();
    }

    void eliminar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(o);
        sesion.getTransaction().commit();
        sesion.close();
    }

}
