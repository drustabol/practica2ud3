package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.Calidades;
import enums.Paises;
import enums.TipoInstrumentos;

import javax.swing.*;


public class Vista extends JFrame {
    private final static String titulo = "Instrumentos";
    private JPanel panel1;

    private String organico;

    JTabbedPane tabbedPane;
    JTextField txtCodigo;
    JTextField txtNombre;
    JTextField txtPvp;
    JComboBox comboTipo;
    JComboBox comboMaterial;

    JButton addInstrumentobtn;
    JButton eliminarInstrumentoBtn;

    DatePicker datePicker;

    JTextField txtNombreMarca;
    JTextField txtDelegacion;
    JTextField txtWeb;
    JComboBox comboPais;
    JButton addMarcaBtn;
    JButton modificarMarcaBtn;
    JButton eliminarMarcaBtn;

    JTextField txtNombreMaterial;
    JComboBox comboCalidad;
    JRadioButton rbtnSi;
    JRadioButton rbtnNo;
    JTextField txtProcedencia;
    JButton addMaterialBtn;
    JButton eliminarMaterialBtn;
    JButton modificarMaterialBtn;
    JButton modificarInstrumentosBtn;

    JButton addPropietarioBtn;
    JButton modificarPropietarioBtn;
    JButton eliminarPropietarioBtn;


    private JScrollPane instrumentosScroll;
    private JScrollPane marcaScroll;
    private JScrollPane materialScroll;

    JList listaInstrumentos;
    JList listaMarcas;
    JList listaMateriales;
    JList listaPropietarios;

    JTextField txtNombrePropietario;
    JTextField txtApellidos;
    JTextField txtDccion;
    JTextField txtTfno;
    JComboBox comboPropietarios;
    JComboBox comboInstrumentos;
    JTextField txtCantidad;
    JButton insertarFabBtn;
    JButton modificarFabBtn;
    JButton borrarFabBtn;
    JList listaFabricados;
    JButton instrumentosBtn;
    JList ListaInstrumentosPropietario;

    private JPanel panelInstrumentos;
    private JPanel panelMarca;
    private JPanel panelMaterial;
    private JPanel panelPropietarios;
    private JPanel panelFabricacion;
     JButton btnlistadoInstrumentos;
     JButton btnListadoMarcas;
     JButton btnlistadoMateriales;
     JButton btnlistadoPropietarios;
     JButton btnListadoFabricados;


    DefaultListModel dlmInstrumentos;
    DefaultListModel dlmMarca;
    DefaultListModel dlmMaterial;
    DefaultListModel dlmPropietario;
    DefaultListModel dlmFabricaciones;
    DefaultListModel dlmInstrumentosPropietario;

    //MENU
    JMenuItem itemSalir;
    JMenuItem conexionItem;


    public Vista() {
        super(titulo);

        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setSize(1440,900);

        this.setVisible(true);
        this.organico="";
        this.setLocationRelativeTo(this);

        crearMenu();
        setEnumCombo();
        setListModels();
    }

    public String getOrganico() {
        return organico;
    }

    public void setOrganico(String organico) {
        this.organico = organico;
    }

    private void crearMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menuArchivo = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        menuArchivo.add(conexionItem);
        menuArchivo.add(itemSalir);

        menuBar.add(menuArchivo);
        menuBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(menuBar);

    }

    private void setEnumCombo() {
        for (Calidades constant : Calidades.values()) {
            comboCalidad.addItem(constant.getValor());

        }
        comboCalidad.setSelectedIndex(-1);

        for (Paises constant : Paises.values()) {
            comboPais.addItem(constant.getValor());
        }
        comboPais.setSelectedIndex(-1);

        for (TipoInstrumentos constant : TipoInstrumentos.values()) {
            comboTipo.addItem(constant.getValor());
        }
        comboTipo.setSelectedIndex(-1);
    }

    private void setListModels() {
        this.dlmInstrumentos = new DefaultListModel();
        this.listaInstrumentos.setModel(dlmInstrumentos);
        this.dlmMarca = new DefaultListModel();
        this.listaMarcas.setModel(dlmMarca);
        this.dlmMaterial = new DefaultListModel();
        this.listaMateriales.setModel(dlmMaterial);
        this.dlmPropietario = new DefaultListModel();
        this.listaPropietarios.setModel(dlmPropietario);
        this.dlmFabricaciones= new DefaultListModel();
        this.listaFabricados.setModel(dlmFabricaciones);
        this.dlmInstrumentosPropietario= new DefaultListModel();
        this.ListaInstrumentosPropietario.setModel(dlmInstrumentosPropietario);

    }
}
