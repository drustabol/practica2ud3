package hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Instrumento {
    private int id;
    private String codigo;
    private String tipo;
    private String nombre;
    private double pvp;
    private Date fechaAdquisicion;
    private Propietario propietario;
    private List<Instrumentomateriales> fabricacion;
    private List<Marca> marca;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "pvp")
    public double getPvp() {
        return pvp;
    }

    public void setPvp(double pvp) {
        this.pvp = pvp;
    }

    @Basic
    @Column(name = "fecha_adquisicion")
    public Date getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(Date fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrumento that = (Instrumento) o;
        return id == that.id && Double.compare(that.pvp, pvp) == 0 && Objects.equals(codigo, that.codigo) && Objects.equals(tipo, that.tipo) && Objects.equals(nombre, that.nombre) && Objects.equals(fechaAdquisicion, that.fechaAdquisicion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, tipo, nombre, pvp, fechaAdquisicion);
    }

    @ManyToOne
    @JoinColumn(name = "idpropietario", referencedColumnName = "id")
    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }

    @OneToMany(mappedBy = "instrumento")
    public List<Instrumentomateriales> getFabricacion() {
        return fabricacion;
    }

    public void setFabricacion(List<Instrumentomateriales> fabricacion) {
        this.fabricacion = fabricacion;
    }

    @ManyToMany
    @JoinTable(name = "marcainstrumento", catalog = "", schema = "bdatos", joinColumns = @JoinColumn(name = "idinstrumento", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "idmarca", referencedColumnName = "id"))
    public List<Marca> getMarca() {
        return marca;
    }

    public void setMarca(List<Marca> marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "Código: " + codigo + " - Tipo: " + tipo + " - " +"Nombre: " + nombre + " - pvp: " + pvp +
                " - Fecha :" + fechaAdquisicion +" - propietario: " + propietario;
    }
}
