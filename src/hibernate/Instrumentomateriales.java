package hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Instrumentomateriales {
    private int id;
    private int cantidad;
    private Instrumento instrumento;
    private Material material;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrumentomateriales that = (Instrumentomateriales) o;
        return id == that.id && cantidad == that.cantidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad);
    }

    @ManyToOne
    @JoinColumn(name = "idinstrumento", referencedColumnName = "id")
    public Instrumento getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }

    @ManyToOne
    @JoinColumn(name = "idmaterial", referencedColumnName = "id")
    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Instrumento: " + instrumento + " - material: " + material +" - cantidad: " + cantidad;
    }
}
