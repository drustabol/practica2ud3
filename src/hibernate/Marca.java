package hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Marca {
    private int id;
    private String nombremarca;
    private String pais;
    private String delegacion;
    private String web;
    private List<Instrumento> instrumento;
    private List<Material> material;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombremarca")
    public String getNombremarca() {
        return nombremarca;
    }

    public void setNombremarca(String nombremarca) {
        this.nombremarca = nombremarca;
    }

    @Basic
    @Column(name = "pais")
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Basic
    @Column(name = "delegacion")
    public String getDelegacion() {
        return delegacion;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    @Basic
    @Column(name = "web")
    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Marca marca = (Marca) o;
        return id == marca.id && Objects.equals(nombremarca, marca.nombremarca) && Objects.equals(pais, marca.pais) && Objects.equals(delegacion, marca.delegacion) && Objects.equals(web, marca.web);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombremarca, pais, delegacion, web);
    }

    @ManyToMany(mappedBy = "marca")
    public List<Instrumento> getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(List<Instrumento> instrumento) {
        this.instrumento = instrumento;
    }

    @ManyToMany
    @JoinTable(name = "marcamaterial", catalog = "", schema = "bdatos", joinColumns = @JoinColumn(name = "idmarca", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "idmaterial", referencedColumnName = "id"))
    public List<Material> getMaterial() {
        return material;
    }

    public void setMaterial(List<Material> material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Nombre: " + nombremarca +" - Pais: " + pais + " - Delegación: " + delegacion +" - web: " + web ;
    }
}
