package hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Material {
    private int id;
    private String nombreMaterial;
    private String calidad;
    private String organico;
    private String procedencia;
    private List<Instrumentomateriales> fabricacion;
    private List<Marca> marca;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombreMaterial")
    public String getNombreMaterial() {
        return nombreMaterial;
    }

    public void setNombreMaterial(String nombreMaterial) {
        this.nombreMaterial = nombreMaterial;
    }

    @Basic
    @Column(name = "calidad")
    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    @Basic
    @Column(name = "organico")
    public String getOrganico() {
        return organico;
    }

    public void setOrganico(String organico) {
        this.organico = organico;
    }

    @Basic
    @Column(name = "procedencia")
    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Material material = (Material) o;
        return id == material.id && Objects.equals(nombreMaterial, material.nombreMaterial) && Objects.equals(calidad, material.calidad) && Objects.equals(organico, material.organico) && Objects.equals(procedencia, material.procedencia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreMaterial, calidad, organico, procedencia);
    }

    @OneToMany(mappedBy = "material")
    public List<Instrumentomateriales> getFabricacion() {
        return fabricacion;
    }

    public void setFabricacion(List<Instrumentomateriales> fabricacion) {
        this.fabricacion = fabricacion;
    }

    @ManyToMany(mappedBy = "material")
    public List<Marca> getMarca() {
        return marca;
    }

    public void setMarca(List<Marca> marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "Material: " + nombreMaterial +" - Calidad: " + calidad + " - organico: " + organico +
                " - Procedencia: " + procedencia;
    }
}
