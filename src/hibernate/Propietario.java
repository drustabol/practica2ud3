package hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Propietario {
    private int id;
    private String nombre;
    private String apellidos;
    private String dccion;
    private int tfno;
    private List<Instrumento> instrumento;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dccion")
    public String getDccion() {
        return dccion;
    }

    public void setDccion(String dccion) {
        this.dccion = dccion;
    }

    @Basic
    @Column(name = "tfno")
    public int getTfno() {
        return tfno;
    }

    public void setTfno(int tfno) {
        this.tfno = tfno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Propietario that = (Propietario) o;
        return id == that.id && tfno == that.tfno && Objects.equals(nombre, that.nombre) && Objects.equals(apellidos, that.apellidos) && Objects.equals(dccion, that.dccion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, dccion, tfno);
    }

    @OneToMany(mappedBy = "propietario")
    public List<Instrumento> getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(List<Instrumento> instrumento) {
        this.instrumento = instrumento;
    }

    @Override
    public String toString() {
        return "Nombre: "+ nombre +" - Apellidos: " + apellidos +" - Dirección: " + dccion + " - Teléfono: " + tfno;
    }
}
